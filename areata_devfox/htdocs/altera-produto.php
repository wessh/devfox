<?php require_once("cabecalho.php");
      require_once("banco-produto.php"); 
      require_once("logica-usuario.php");?>

<?php
$id = $_POST['id'];
$nome = $_POST["nome"];
$preco = $_POST["preco"];
$descricao = $_POST["descricao"];
$categoria_id = $_POST['categoria_id'];
if(array_key_exists('usado',$_POST)){
  $usado = "true";
}else{
  $usado = "false";
}

if(alteraProduto($conexao, $id, $nome, $preco, $descricao, $categoria_id, $usado)) { 
  $_SESSION["success"] = "O produto alterado com sucesso!";
	header("Location: produto-altera-formulario.php");
 } else {
  $msg = mysqli_error($conexao);
  $_SESSION["danger"] = "O produto não foi alterado!";
	header("Location: produto-altera-formulario.php");
}
die();
?>

<?php include("rodape.php"); ?>
