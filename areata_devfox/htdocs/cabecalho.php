<?php require_once("alertas.php");
require_once("logica-usuario.php"); 
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Minha loja</title>
    <meta charset="utf-8">
  
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <link href="css/loja.css" rel="stylesheet" />
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">Minha Loja</a>
    <div class="navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <?php
        if(usuarioEstaLogado()){
        ?>
            <li class="nav-item">
                <a class="nav-link" href="produto-formulario.php">Adiciona Produto</a>
            </li>
            <?php
        }
        ?>
            <li class="nav-item">
                <a class="nav-link" href="produto-lista.php">Produtos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contato.php">Contato</a>
            </li>
        </ul>
    </div>
</nav>


    <div class="container">

        <div class="principal">
<?php
mostraAlerta("success");
mostraAlerta("danger");
?>