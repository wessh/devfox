<?php

session_start();

function usuarioEstaLogado()
{
    return isset($_SESSION["usuario_logado"]);
}
function usuarioLogado()
{
    return $_SESSION["usuario_logado"];
}

function logaUsuario($email)
{
    $_SESSION["usuario_logado"] = $email;
}

function verificaUsuario()
{
    if (!usuarioLogado()) {
        $_SESSION["danger"] = "Acesso recusado por falta de permissão!";
        header("Location: index.php");
        die();
    }
}

function logout(){
    session_destroy();
    session_start();
}