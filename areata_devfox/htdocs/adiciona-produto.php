<?php require_once("cabecalho.php");
      require_once("banco-produto.php"); 
      require_once("logica-usuario.php");?>

<?php
verificaUsuario();
$nome = $_POST["nome"];
$preco = $_POST["preco"];
$descricao = $_POST["descricao"];
$categoria_id = $_POST['categoria_id'];
if(array_key_exists('usado',$_POST)){
  $usado = "true";
}else{
  $usado = "false";
}

if(insereProduto($conexao, $nome, $preco, $descricao, $categoria_id, $usado)) { 
  $_SESSION["success"] = "O produto adicionado com sucesso!";
	header("Location: produto-formulario.php");
 } else {
  $msg = mysqli_error($conexao);
  $_SESSION["danger"] = "O produto não foi adicionado!";
	header("Location: produto-formulario.php");
}
?>

<?php include("rodape.php"); ?>
